package main.java.helpers;

import java.util.Date;

public interface DateHelper {
    Date getToday();
}
