package main.java.helpers;

import main.java.VoucherType;
import main.java.elements.Company;
import main.java.elements.User;
import main.java.elements.Vouchers;
import main.java.helpers.dto.*;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class JsonHelper {

    public List<Company> getCompanies(Elements elements){
        List<Company> companies = new ArrayList<>();
        for(CompanyDTO company: elements.companies){
            Company newCompany = new Company(company.id, company.name, company.balance);
            companies.add(newCompany);
        }
        return companies;
    }

    public List<User> getUsers(Elements elements){
        List<User> users = new ArrayList<>();
        for(UserDTO user: elements.users){
            User newUser = new User(user.id, getVouchersFromAmounts(user.balance,elements.wallets));
            users.add(newUser);
        }
        return users;
    }

    public List<UserDTO> getUsersDTO(List<User> usersList,List<WalletDTO> wallets){
        List<UserDTO> users = new ArrayList<>();
        for(User user: usersList){
            UserDTO newUser = new UserDTO(user.getId(),getAmountsFromVouchers(user.getVouchers(),wallets));
            users.add(newUser);
        }
        return users;
    }

    public List<CompanyDTO> getCompanyDTO(List<Company> companyList){
        List<CompanyDTO> companies = new ArrayList<>();
        for(Company company: companyList){
            CompanyDTO newCompany = new CompanyDTO(company.getId(),company.getName(),company.getBalance());
            companies.add(newCompany);
        }
        return companies;
    }



    public List<Vouchers> getVouchersFromAmounts(List<Amounts> amounts, List<WalletDTO> wallets){
        List<Vouchers> vouchers = new ArrayList<>();
        for(Amounts amount : amounts){
            vouchers.add(new Vouchers(amount.amount,Calendar.getInstance().getTime(), getWalletFromList(amount.wallet_id,wallets).type));
        }
        return vouchers;
    }

    public List<Amounts> getAmountsFromVouchers(List<Vouchers> vouchers, List<WalletDTO> wallets){
        List<Amounts> amounts = new ArrayList<>();
        for(Vouchers voucher : vouchers){
            amounts.add(new Amounts(getWalletFromType(voucher.getType(),wallets).id,voucher.getAmount()));
        }
        return amounts;
    }

    private WalletDTO getWalletFromList(int id, List<WalletDTO> wallets){
        for(WalletDTO wallet : wallets){
            if(wallet.id == id) return wallet;
        }
        return new WalletDTO();
    }

    private WalletDTO getWalletFromType(VoucherType type, List<WalletDTO> wallets){
        for(WalletDTO wallet : wallets){
            if(wallet.type == type) return wallet;
        }
        return new WalletDTO();
    }

}