package main.java.helpers.dto;

import java.util.List;

public class Elements {
    public List<WalletDTO> wallets;
    public List<CompanyDTO> companies;
    public List<UserDTO> users;
    public List<Distributions> distributions;

    public Elements(List<WalletDTO> wallets, List<CompanyDTO> companies, List<UserDTO> users, List<Distributions> distributions) {
        this.wallets = wallets;
        this.companies = companies;
        this.users = users;
        this.distributions = distributions;
    }
}

