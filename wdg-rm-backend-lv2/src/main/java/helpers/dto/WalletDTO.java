package main.java.helpers.dto;

import main.java.VoucherType;

public class WalletDTO {
    public int id;
    public String name;
    public VoucherType type;
}
