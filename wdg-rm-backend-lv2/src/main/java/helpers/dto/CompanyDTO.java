package main.java.helpers.dto;

public class CompanyDTO {
    public int id;
    public String name;
    public int balance;

    public CompanyDTO(int id, String name, int balance) {
        this.id = id;
        this.name = name;
        this.balance = balance;
    }
}
