package main.java.helpers.dto;

public class Amounts {
    public int wallet_id;
    public int amount;

    public Amounts(int wallet_id, int amount) {
        this.wallet_id = wallet_id;
        this.amount = amount;
    }
}
