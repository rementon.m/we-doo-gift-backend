package main.java.helpers.dto;

import java.util.List;

public class UserDTO {
    public int id;
    public List<Amounts> balance;

    public UserDTO(int id, List<Amounts> balance) {
        this.id = id;
        this.balance = balance;
    }
}

