package main.java.helpers;

import com.google.gson.Gson;
import main.java.elements.Company;
import main.java.elements.User;
import main.java.helpers.dto.Elements;
import main.java.helpers.dto.WalletDTO;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

public class FileService {
    private Elements elements;

    public FileService(String filename) {
        try {
            File file = new File(filename);
            StringBuilder fileContents = new StringBuilder((int) file.length());
            try (Scanner scanner = new Scanner(file)) {
                while (scanner.hasNextLine()) {
                    fileContents.append(scanner.nextLine()).append(System.lineSeparator());
                }
                Gson gson = new Gson();
                elements = gson.fromJson(fileContents.toString(), Elements.class);
            }
        } catch (FileNotFoundException e) {
            System.out.println("File " + filename + " not found.");
            e.printStackTrace();
        }
    }

    public boolean generateOutputFile(Elements input){
        try {
            Gson gson = new Gson();
            String json = gson.toJson(input);
            FileWriter myWriter = new FileWriter("output.json");
            myWriter.write(json);
            myWriter.close();
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public List<Company> getCompanies(){
        JsonHelper jsonHelper = new JsonHelper();
        return jsonHelper.getCompanies(elements);
    }

    public List<User> getUsers(){
        JsonHelper jsonHelper = new JsonHelper();
        return jsonHelper.getUsers(elements);
    }

    public List<WalletDTO> getWallets(){
        return elements.wallets;
    }

}

