package main.java.elements;

import main.java.VoucherType;
import main.java.exception.NotEnoughFundsException;
import main.java.helpers.DateHelper;
import main.java.helpers.DateHelperImpl;

import java.util.Date;

public class Company {
    private final int id;
    private final String name;
    private int balance;
    private final DateHelper dateHelper;

    public Company(int id, String name, int balance){
        this.id = id;
        this.name = name;
        this.balance = balance;
        this.dateHelper = new DateHelperImpl();
    }

    public Company(String name, int balance){
        this.id = 0;
        this.name = name;
        this.balance = balance;
        this.dateHelper = new DateHelperImpl();
    }

    public int getBalance() {
        return balance;
    }

    public Date distributeVouchers(User user, int amount, VoucherType voucherType) throws NotEnoughFundsException {
        return this.distributeVouchers(user,amount,voucherType,dateHelper.getToday());
    }

    public Date distributeVouchers(User user, int amount, VoucherType voucherType, Date startDate) throws NotEnoughFundsException {
        if(this.balance - amount >= 0) {
            Vouchers voucher = new Vouchers(amount, startDate, voucherType);
            user.getVoucher(voucher);
            this.balance = this.balance - amount;
            return voucher.getEndDate();
        }
        else throw new NotEnoughFundsException();
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}

