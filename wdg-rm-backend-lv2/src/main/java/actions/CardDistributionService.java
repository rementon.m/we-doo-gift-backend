package main.java.actions;


import main.java.elements.Company;
import main.java.elements.User;
import main.java.exception.NotEnoughFundsException;
import main.java.helpers.DateHelper;
import main.java.helpers.DateHelperImpl;
import main.java.helpers.dto.Distributions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static main.java.VoucherType.GIFT;
import static main.java.VoucherType.FOOD;


public class CardDistributionService {
    private final List<Distributions> distributionsList = new ArrayList<>();
    private int counter = 0;
    private final DateHelper dateHelper = new DateHelperImpl();

    public void distributeGiftCard(Company company, User user, int amount){
        this.distributeGiftCard(company, user, amount,dateHelper.getToday());
    }

    public void distributeMealVouchers(Company company, User user, int amount){
        this.distributeMealVouchers(company, user, amount, dateHelper.getToday());
    }

    public void distributeGiftCard(Company company, User user, int amount, Date startDate){
        try{
            final Date endDate = company.distributeVouchers(user, amount, GIFT, startDate);
            String start = new SimpleDateFormat("yyyy-MM-dd").format(startDate);
            String end = new SimpleDateFormat("yyyy-MM-dd").format(endDate);
            counter = counter + 1;
            Distributions distributions =
                    new Distributions(counter,amount,start,end,company.getId(),user.getId());
            distributionsList.add(distributions);
        } catch (NotEnoughFundsException ignored){}
    }

    public void distributeMealVouchers(Company company, User user, int amount, Date startDate){
        try{
            final Date endDate = company.distributeVouchers(user, amount, FOOD, startDate);
            String start = new SimpleDateFormat("yyyy-MM-dd").format(startDate);
            String end = new SimpleDateFormat("yyyy-MM-dd").format(endDate);
            counter = counter + 1;
            Distributions distributions =
                    new Distributions(counter,amount,start,end,company.getId(),user.getId());
            distributionsList.add(distributions);
        } catch (NotEnoughFundsException ignored){}
    }

    public List<Distributions> getDistributionsList(){
        return distributionsList;
    }


}
