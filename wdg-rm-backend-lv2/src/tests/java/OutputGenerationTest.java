package tests.java;

import main.java.actions.CardDistributionService;
import main.java.elements.Company;
import main.java.elements.User;
import main.java.helpers.FileService;
import main.java.helpers.JsonHelper;
import main.java.helpers.dto.CompanyDTO;
import main.java.helpers.dto.Elements;
import main.java.helpers.dto.UserDTO;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static main.java.VoucherType.FOOD;
import static main.java.VoucherType.GIFT;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class OutputGenerationTest {

    CardDistributionService cardDistributionService = new CardDistributionService();

    FileService input = new FileService("input.json");

    JsonHelper jsonHelper = new JsonHelper();

    @Test
    void expectedOutputGeneratedScenario() {
        // GENERATION
        Company company1 = input.getCompanies().get(0);
        Company company2 = input.getCompanies().get(1);

        User user1 = input.getUsers().get(0);
        User user2 = input.getUsers().get(1);
        User user3 = input.getUsers().get(2);

        // DISTRIBUTION
        cardDistributionService.distributeGiftCard(company1,user1,50);
        cardDistributionService.distributeGiftCard(company1,user2,100);
        cardDistributionService.distributeMealVouchers(company1,user1,250);

        cardDistributionService.distributeGiftCard(company2,user3,1000);

        // EXPECTATIONS
        assertEquals(150,user1.getBalanceFor(GIFT));
        assertEquals(250,user1.getBalanceFor(FOOD));

        assertEquals(100,user2.getBalanceFor(GIFT));
        assertEquals(0,user2.getBalanceFor(FOOD));

        assertEquals(1000,user3.getBalanceFor(GIFT));
        assertEquals(0,user3.getBalanceFor(FOOD));

        assertEquals(600,company1.getBalance());
        assertEquals(2000,company2.getBalance());
    }

    @Test
    void expectedOutputFileGeneratedScenario() {
        // GENERATION
        List<Company> companies = new ArrayList<>();
        Company company1 = input.getCompanies().get(0);
        companies.add(company1);
        Company company2 = input.getCompanies().get(1);
        companies.add(company2);

        List<User> users = new ArrayList<>();
        User user1 = input.getUsers().get(0);
        users.add(user1);
        User user2 = input.getUsers().get(1);
        users.add(user2);
        User user3 = input.getUsers().get(2);
        users.add(user3);

        // DISTRIBUTION
        cardDistributionService.distributeGiftCard(company1,user1,50);
        cardDistributionService.distributeGiftCard(company1,user2,100);
        cardDistributionService.distributeMealVouchers(company1,user1,250);

        cardDistributionService.distributeGiftCard(company2,user3,1000);

        // GENERATION
        List<UserDTO> userDTOList = jsonHelper.getUsersDTO(users,input.getWallets());
        List<CompanyDTO> companyDTOList = jsonHelper.getCompanyDTO(companies);
        Elements outputContent = new Elements(input.getWallets(),companyDTOList,userDTOList,
                cardDistributionService.getDistributionsList());
        assertTrue(input.generateOutputFile(outputContent));

        FileService output = new FileService("output.json");

        // EXPECTATIONS
        assertEquals(150, output.getUsers().get(0).getBalanceFor(GIFT));
        assertEquals(250, output.getUsers().get(0).getBalanceFor(FOOD));

        assertEquals(100, output.getUsers().get(1).getBalanceFor(GIFT));
        assertEquals(0, output.getUsers().get(1).getBalanceFor(FOOD));

        assertEquals(1000, output.getUsers().get(2).getBalanceFor(GIFT));
        assertEquals(0, output.getUsers().get(2).getBalanceFor(FOOD));

        assertEquals(600, output.getCompanies().get(0).getBalance());
        assertEquals(2000, output.getCompanies().get(1).getBalance());
    }

}
