package tests.java;

import main.java.VoucherType;
import main.java.elements.Vouchers;
import main.java.helpers.DateHelper;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Calendar;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class VouchersTest {

    /*** GIFT CARD VOUCHERS TYPE TEST ***/

    @Test
    void validGiftCardReturnExpectedAmount() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2020);
        cal.set(Calendar.MONTH, Calendar.DECEMBER);
        cal.set(Calendar.DAY_OF_MONTH, 4);
        Date start = cal.getTime();

        cal.set(Calendar.YEAR, 2021);
        cal.set(Calendar.MONTH, Calendar.MARCH);
        cal.set(Calendar.DAY_OF_MONTH, 2);
        Date today = cal.getTime();

        final DateHelper dateHelper = Mockito.mock(DateHelper.class);
        Mockito.when(dateHelper.getToday()).thenReturn(today);

        Vouchers card = new Vouchers(35,start, VoucherType.GIFT,dateHelper);

        assertEquals(35,card.getAmount());
    }

    @Test
    void beforeNonValidGiftCardReturnZero() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2020);
        cal.set(Calendar.MONTH, Calendar.DECEMBER);
        cal.set(Calendar.DAY_OF_MONTH, 4);
        Date start = cal.getTime();

        cal.set(Calendar.YEAR, 2021);
        cal.set(Calendar.MONTH, Calendar.DECEMBER);
        cal.set(Calendar.DAY_OF_MONTH, 3);
        Date end = cal.getTime();

        cal.set(Calendar.YEAR, 2020);
        cal.set(Calendar.MONTH, Calendar.DECEMBER);
        cal.set(Calendar.DAY_OF_MONTH, 3);
        Date today = cal.getTime();

        final DateHelper dateHelper = Mockito.mock(DateHelper.class);
        Mockito.when(dateHelper.getToday()).thenReturn(today);

        Vouchers card = new Vouchers(35,start,VoucherType.GIFT,dateHelper);

        assertEquals(0,card.getAmount());
        assertEquals(end,card.getEndDate());
    }

    @Test
    void afterNonValidGiftCardReturnZero() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2020);
        cal.set(Calendar.MONTH, Calendar.APRIL);
        cal.set(Calendar.DAY_OF_MONTH, 17);
        Date start = cal.getTime();

        cal.set(Calendar.YEAR, 2021);
        cal.set(Calendar.MONTH, Calendar.APRIL);
        cal.set(Calendar.DAY_OF_MONTH, 20);
        Date today = cal.getTime();

        final DateHelper dateHelper = Mockito.mock(DateHelper.class);
        Mockito.when(dateHelper.getToday()).thenReturn(today);

        Vouchers card = new Vouchers(35,start,VoucherType.GIFT,dateHelper);

        assertEquals(0,card.getAmount());
    }

    @Test
    void includedDateValidGiftCardReturnExpectedAmount() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2020);
        cal.set(Calendar.MONTH, Calendar.DECEMBER);
        cal.set(Calendar.DAY_OF_MONTH, 4);
        Date start = cal.getTime();

        cal.set(Calendar.YEAR, 2020);
        cal.set(Calendar.MONTH, Calendar.DECEMBER);
        cal.set(Calendar.DAY_OF_MONTH, 4);
        Date today = cal.getTime();

        final DateHelper dateHelper = Mockito.mock(DateHelper.class);
        Mockito.when(dateHelper.getToday()).thenReturn(today);

        Vouchers card = new Vouchers(35,start,VoucherType.GIFT,dateHelper);

        assertEquals(35,card.getAmount());
    }

    /*** MEAL VOUCHERS TYPE TEST ***/

    @Test
    void createdMealVoucherHasExpectedEndDate() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2020);
        cal.set(Calendar.MONTH, Calendar.DECEMBER);
        cal.set(Calendar.DAY_OF_MONTH, 4);
        Date start = cal.getTime();

        cal.set(Calendar.YEAR, 2021);
        cal.set(Calendar.MONTH, Calendar.FEBRUARY);
        cal.set(Calendar.DAY_OF_MONTH, 28);
        Date end = cal.getTime();

        Vouchers mealVoucher = new Vouchers(35,start, VoucherType.FOOD);

        assertEquals(end,mealVoucher.getEndDate());
    }
}