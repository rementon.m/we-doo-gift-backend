package tests.java;

import main.java.elements.User;
import main.java.elements.Vouchers;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static java.util.Collections.emptyList;
import static main.java.VoucherType.FOOD;
import static main.java.VoucherType.GIFT;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserTest {
    private Date today = Calendar.getInstance().getTime();

    @Test
    void initializedUserWithEmptyGiftCardsHasCorrectAmount() {

        User user = new User(emptyList());

        assertEquals(0,user.getBalanceFor(GIFT));
        assertEquals(0,user.getBalanceFor(FOOD));
    }

    @Test
    void initializedUserWithVouchersHasCorrectAmount() {

        List<Vouchers> vouchersList = new ArrayList<>();
        vouchersList.add(new Vouchers(345,today,GIFT));
        vouchersList.add(new Vouchers(459,today,FOOD));

        User user = new User(vouchersList);

        assertEquals(345,user.getBalanceFor(GIFT));
        assertEquals(459,user.getBalanceFor(FOOD));
    }

    @Test
    void userUpdateVouchersReturnCorrectAmount() {

        List<Vouchers> vouchersList = new ArrayList<>();
        vouchersList.add(new Vouchers(350,today,GIFT));

        User user = new User(vouchersList);
        user.getVoucher(new Vouchers(50,today,GIFT));

        assertEquals(400,user.getBalanceFor(GIFT));
    }
}
