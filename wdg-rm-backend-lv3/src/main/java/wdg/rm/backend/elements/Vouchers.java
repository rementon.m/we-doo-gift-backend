package wdg.rm.backend.elements;

import wdg.rm.backend.VoucherType;
import wdg.rm.backend.helpers.DateHelper;
import wdg.rm.backend.helpers.DateHelperImpl;

import java.util.Calendar;
import java.util.Date;

public class Vouchers {
    private int amount;
    private DateHelper dateHelper;
    private Date startDate;
    private Date endDate;
    private VoucherType voucherType;

    public Vouchers(int amount, Date startDate, VoucherType voucherType, DateHelper dateHelper) {
        this.amount = amount;
        this.dateHelper = dateHelper;
        this.startDate = startDate;
        this.voucherType = voucherType;
        this.endDate = generateEndDate();
    }

    public Vouchers(int amount, Date startDate, VoucherType voucherType) {
        this.amount = amount;
        this.startDate = startDate;
        this.voucherType = voucherType;
        this.endDate = generateEndDate();
        this.dateHelper = new DateHelperImpl();
    }

    public int getAmount(){
        if(!startDate.after(dateHelper.getToday()) && !endDate.before(dateHelper.getToday())) return amount;
        else return 0;
    }

    public Date getEndDate() {
        return endDate;
    }

    public VoucherType getType() {
        return voucherType;
    }

    private Date generateEndDate(){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);
        calendar.add(Calendar.YEAR, 1);
        if(voucherType.equals(VoucherType.GIFT)){
            calendar.add(Calendar.DAY_OF_MONTH, -1);
        }
        if(voucherType.equals(VoucherType.FOOD)){
            calendar.set(Calendar.MONTH, Calendar.FEBRUARY);
            calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        }
        return calendar.getTime();
    }
}
