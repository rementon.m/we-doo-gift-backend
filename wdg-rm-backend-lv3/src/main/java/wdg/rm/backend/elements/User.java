package wdg.rm.backend.elements;

import wdg.rm.backend.VoucherType;

import java.util.ArrayList;
import java.util.List;

public class User {
    private final List<Vouchers> vouchers = new ArrayList<>();
    private final int id;

    public User(List<Vouchers> initialBalance) {
        this.id = 0;
        vouchers.addAll(initialBalance);
    }

    public User(int id,List<Vouchers> initialBalance) {
        this.id = id;
        vouchers.addAll(initialBalance);
    }

    public int getBalanceFor(VoucherType type) {
        int actualAmount = 0;
        for(Vouchers card : vouchers){
            if(card.getType().equals(type)) actualAmount += card.getAmount();
        }
        return actualAmount;
    }

    public void getVoucher(Vouchers card) {
        vouchers.add(card);
    }

    public int getId() {
        return id;
    }

    public List<Vouchers> getVouchers(){
        return vouchers;
    }
}