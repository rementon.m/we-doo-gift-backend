package wdg.rm.backend.helpers;

import java.util.Calendar;
import java.util.Date;

public class DateHelperImpl implements DateHelper{

    @Override
    public Date getToday() {
        return Calendar.getInstance().getTime();
    }
}
