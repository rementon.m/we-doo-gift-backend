package wdg.rm.backend.helpers;

import java.util.Date;

public interface DateHelper {
    Date getToday();
}
