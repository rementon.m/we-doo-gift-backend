package wdg.rm.backend.converters;

import org.springframework.stereotype.Component;
import wdg.rm.backend.elements.Company;
import wdg.rm.backend.repository.dto.CompanyDTO;

@Component
public class CompanyConverter {

    public Company getCompanyFromCompanyDTO(CompanyDTO company){
        return new Company(company.id,company.name, company.balance);
    }

    public CompanyDTO getCompanyDTOFromCompany(Company company){
        return new CompanyDTO(company.getId(),company.getName(),company.getBalance());
    }
}
