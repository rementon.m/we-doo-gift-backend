package wdg.rm.backend.converters;

import org.springframework.stereotype.Component;
import wdg.rm.backend.VoucherType;
import wdg.rm.backend.elements.User;
import wdg.rm.backend.elements.Vouchers;
import wdg.rm.backend.repository.dto.*;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Component
public class UserConverter {

    public User getUserFromUserDTO(UserDTO userDto,List<WalletDTO> wallets){
        return new User(userDto.id, getVouchersFromAmounts(userDto.balance,wallets));
    }

    public List<Vouchers> getVouchersFromAmounts(List<Amounts> amounts, List<WalletDTO> wallets){
        List<Vouchers> vouchers = new ArrayList<>();
        for(Amounts amount : amounts){
            vouchers.add(new Vouchers(amount.amount,Calendar.getInstance().getTime(), getWalletFromList(amount.wallet_id,wallets).type));
        }
        return vouchers;
    }

    private WalletDTO getWalletFromList(int id, List<WalletDTO> wallets){
        for(WalletDTO wallet : wallets){
            if(wallet.id == id) return wallet;
        }
        return new WalletDTO();
    }

    public UserDTO getUserDTOFromUser(User user,List<WalletDTO> wallets) {
        return new UserDTO(user.getId(),getAmountsFromVouchers(user.getVouchers(),wallets));
    }

    public List<Amounts> getAmountsFromVouchers(List<Vouchers> vouchers, List<WalletDTO> wallets){
        List<Amounts> amounts = new ArrayList<>();
        for(Vouchers voucher : vouchers){
            amounts.add(new Amounts(getWalletFromType(voucher.getType(),wallets).id,voucher.getAmount()));
        }
        return amounts;
    }

    private WalletDTO getWalletFromType(VoucherType type, List<WalletDTO> wallets){
        for(WalletDTO wallet : wallets){
            if(wallet.type == type) return wallet;
        }
        return new WalletDTO();
    }
}