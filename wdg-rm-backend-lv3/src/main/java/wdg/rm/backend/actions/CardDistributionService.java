package wdg.rm.backend.actions;

import org.springframework.stereotype.Service;
import wdg.rm.backend.elements.Company;
import wdg.rm.backend.elements.User;
import wdg.rm.backend.exceptions.NotEnoughFundsException;
import wdg.rm.backend.helpers.DateHelper;
import wdg.rm.backend.helpers.DateHelperImpl;
import wdg.rm.backend.repository.dto.Distributions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static wdg.rm.backend.VoucherType.GIFT;
import static wdg.rm.backend.VoucherType.FOOD;

@Service
public class CardDistributionService {
    private final List<Distributions> distributionsList = new ArrayList<>();
    private int counter = 0;
    private final DateHelper dateHelper = new DateHelperImpl();

    public void distributeGiftCard(Company company, User user, int amount){
        this.distributeGiftCard(company, user, amount,dateHelper.getToday());
    }

    public void distributeMealVouchers(Company company, User user, int amount){
        this.distributeMealVouchers(company, user, amount, dateHelper.getToday());
    }

    public void distributeGiftCard(Company company, User user, int amount, Date startDate){
        try{
            final Date endDate = company.distributeVouchers(user, amount, GIFT, startDate);
            String start = new SimpleDateFormat("yyyy-MM-dd").format(startDate);
            String end = new SimpleDateFormat("yyyy-MM-dd").format(endDate);
            counter = counter + 1;
            Distributions distributions =
                    new Distributions(counter,amount,start,end,company.getId(),user.getId());
            distributionsList.add(distributions);
        } catch (NotEnoughFundsException ignored){}
    }

    public void distributeMealVouchers(Company company, User user, int amount, Date startDate){
        try{
            final Date endDate = company.distributeVouchers(user, amount, FOOD, startDate);
            String start = new SimpleDateFormat("yyyy-MM-dd").format(startDate);
            String end = new SimpleDateFormat("yyyy-MM-dd").format(endDate);
            counter = counter + 1;
            Distributions distributions =
                    new Distributions(counter,amount,start,end,company.getId(),user.getId());
            distributionsList.add(distributions);
        } catch (NotEnoughFundsException ignored){}
    }

    public List<Distributions> getDistributionsList(){
        return distributionsList;
    }

}