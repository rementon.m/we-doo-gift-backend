package wdg.rm.backend;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import wdg.rm.backend.repository.dto.Amounts;
import wdg.rm.backend.repository.dto.UserDTO;
import wdg.rm.backend.repository.dto.WalletDTO;
import wdg.rm.backend.repository.UserRepository;
import wdg.rm.backend.repository.WalletRepository;

import java.util.ArrayList;
import java.util.List;

@Configuration
class PreLoadDbData {

    private static final Logger log = LoggerFactory.getLogger(PreLoadDbData.class);

    @Bean
    CommandLineRunner initWalletDatabase(WalletRepository repository) {
        return args -> {
            log.info("initializing... " + repository.save(new WalletDTO(1,"gift cards",VoucherType.GIFT)));
            log.info("initializing... " + repository.save(new WalletDTO(2,"food cards",VoucherType.FOOD)));
        };
    }


    @Bean
    CommandLineRunner initUserDatabase(UserRepository repository) {
        return args -> {
            List<Amounts> amountsList = new ArrayList<>();
            amountsList.add(new Amounts(1,560));
            log.info("initializing... " + repository.save(new UserDTO(1,amountsList)));
        };
    }
}