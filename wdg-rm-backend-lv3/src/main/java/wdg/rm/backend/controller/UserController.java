package wdg.rm.backend.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import wdg.rm.backend.exceptions.UserNotFoundException;
import wdg.rm.backend.repository.dto.UserDTO;
import wdg.rm.backend.repository.UserRepository;

import java.util.List;

@Secured("ROLE_ADMIN")
@RestController
public class UserController {

    private final UserRepository repository;

    public UserController(UserRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/users")
    private List<UserDTO> getAllUsers() {
        return repository.findAll();
    }

    @PostMapping("/users")
    UserDTO addNewUser(@RequestBody UserDTO newUser) {
        return repository.save(newUser);
    }

    @GetMapping("/users/{id}")
    UserDTO getUserInformation(@PathVariable int id) {
        return repository.findById(id).orElseThrow(() -> new UserNotFoundException(id));
    }

    @PutMapping("/users/{id}")
    UserDTO replaceUser(@RequestBody UserDTO newUser, @PathVariable int id) {
        return repository.findById(id)
                .map(user -> {
                    user.id = newUser.id;
                    user.balance = newUser.balance;
                    return repository.save(user);
                })
                .orElseGet(() -> {
                    newUser.id = id;
                    return repository.save(newUser);
                });
    }

    @DeleteMapping("/users/{id}")
    void deleteUser(@PathVariable int id) {
        repository.deleteById(id);
    }
}

