package wdg.rm.backend.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import wdg.rm.backend.actions.CardDistributionService;
import wdg.rm.backend.converters.CompanyConverter;
import wdg.rm.backend.converters.UserConverter;
import wdg.rm.backend.elements.Company;
import wdg.rm.backend.elements.User;
import wdg.rm.backend.exceptions.UserNotFoundException;
import wdg.rm.backend.repository.UserRepository;
import wdg.rm.backend.repository.CompanyRepository;
import wdg.rm.backend.repository.WalletRepository;
import wdg.rm.backend.repository.dto.CompanyDTO;
import wdg.rm.backend.repository.dto.UserDTO;

import java.util.NoSuchElementException;

import static wdg.rm.backend.VoucherType.FOOD;
import static wdg.rm.backend.VoucherType.GIFT;

@Secured({"ROLE_USER","ROLE_ADMIN"})
@RestController
public class VouchersController {

    private final UserRepository userRepository;
    private final CompanyRepository companyRepository;
    private final WalletRepository wallets;
    private final CardDistributionService distributionService;
    private final UserConverter userConverter;
    private final CompanyConverter companyConverter;

    public VouchersController(UserRepository userRepository, CompanyRepository companyRepository,
                              WalletRepository wallets, CardDistributionService distribution,
                              UserConverter userConverter, CompanyConverter companyConverter) {
        this.userRepository = userRepository;
        this.companyRepository = companyRepository;
        this.wallets = wallets;
        this.distributionService = distribution;
        this.userConverter = userConverter;
        this.companyConverter = companyConverter;
    }

    @PostMapping("/giftCards/{amount}/{userId}")
    CompanyDTO distributeGiftCards(@RequestBody CompanyDTO request, @PathVariable int amount,
                                   @PathVariable int userId) {
        CompanyDTO companyDTO;
        try{
            companyDTO = companyRepository.findById(request.id).orElseThrow();
        }catch (NoSuchElementException e){
            companyDTO = companyRepository.save(request);
        }

        UserDTO userDTO = userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId));

        // Convert
        Company company = companyConverter.getCompanyFromCompanyDTO(companyDTO);
        User user = userConverter.getUserFromUserDTO(userDTO,wallets.findAll());

        //distribute
        distributionService.distributeGiftCard(company,user,amount);

        userDTO = userConverter.getUserDTOFromUser(user,wallets.findAll());
        UserDTO finalUserDTO = userDTO;
        userRepository.findById(finalUserDTO.id).map(updatedUser -> {
            updatedUser.id = finalUserDTO.id;
            updatedUser.balance = finalUserDTO.balance;
            return userRepository.save(updatedUser);
        });

        companyDTO = companyConverter.getCompanyDTOFromCompany(company);
        CompanyDTO finalCompanyDTO = companyDTO;
        companyRepository.findById(finalCompanyDTO.id).map(updatedCompany -> {
            updatedCompany.id = finalCompanyDTO.id;
            updatedCompany.name = finalCompanyDTO.name;
            updatedCompany.balance = finalCompanyDTO.balance;
            return companyRepository.save(updatedCompany);
        });

        return companyDTO;
    }

    @PostMapping("/mealVouchers/{amount}/{userId}")
    CompanyDTO distributeMealVouchers(@RequestBody CompanyDTO request, @PathVariable int amount,
                                   @PathVariable int userId) {
        CompanyDTO companyDTO;
        try{
            companyDTO = companyRepository.findById(request.id).orElseThrow();
        }catch (NoSuchElementException e){
            companyDTO = companyRepository.save(request);
        }

        UserDTO userDTO = userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId));

        // Convert
        Company company = companyConverter.getCompanyFromCompanyDTO(companyDTO);
        User user = userConverter.getUserFromUserDTO(userDTO,wallets.findAll());

        //distribute
        distributionService.distributeMealVouchers(company,user,amount);

        userDTO = userConverter.getUserDTOFromUser(user,wallets.findAll());
        UserDTO finalUserDTO = userDTO;
        userRepository.findById(finalUserDTO.id).map(updatedUser -> {
            updatedUser.id = finalUserDTO.id;
            updatedUser.balance = finalUserDTO.balance;
            return userRepository.save(updatedUser);
        });

        companyDTO = companyConverter.getCompanyDTOFromCompany(company);
        CompanyDTO finalCompanyDTO = companyDTO;
        companyRepository.findById(finalCompanyDTO.id).map(updatedCompany -> {
            updatedCompany.id = finalCompanyDTO.id;
            updatedCompany.name = finalCompanyDTO.name;
            updatedCompany.balance = finalCompanyDTO.balance;
            return companyRepository.save(updatedCompany);
        });

        return companyDTO;
    }

    @GetMapping("/vouchers/{userId}")
    int getUserBalance(@PathVariable int userId) {
        UserDTO userDTO = userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId));
        User user = userConverter.getUserFromUserDTO(userDTO,wallets.findAll());
        return user.getBalanceFor(GIFT) + user.getBalanceFor(FOOD);
    }
}