package wdg.rm.backend.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import wdg.rm.backend.repository.dto.WalletDTO;
import wdg.rm.backend.repository.WalletRepository;

import java.util.List;

@Secured("ROLE_ADMIN")
@RestController
public class WalletsController {

    private final WalletRepository repository;

    public WalletsController(WalletRepository repository) {
        this.repository = repository;
    }

    @PostMapping("/wallet")
    WalletDTO addNewWallet(@RequestBody WalletDTO newWallet) {
        return repository.save(newWallet);
    }

    @GetMapping("/wallet")
    List<WalletDTO> getAllWallets() {
        return repository.findAll();
    }
}

