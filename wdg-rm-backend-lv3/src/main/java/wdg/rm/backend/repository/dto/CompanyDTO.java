package wdg.rm.backend.repository.dto;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class CompanyDTO {
    public @Id
    int id;
    public String name;
    public int balance;

    public CompanyDTO(int id, String name, int balance) {
        this.id = id;
        this.name = name;
        this.balance = balance;
    }

    public CompanyDTO(){}
}
