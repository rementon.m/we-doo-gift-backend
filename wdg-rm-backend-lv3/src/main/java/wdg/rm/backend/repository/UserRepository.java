package wdg.rm.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import wdg.rm.backend.repository.dto.UserDTO;

public interface UserRepository extends JpaRepository<UserDTO, Integer> { }