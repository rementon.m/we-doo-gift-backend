package wdg.rm.backend.repository.dto;

import java.util.List;

public class Elements {
    public List<WalletDTO> wallets;
    public List<CompanyDTO> companies;
    public List<UserDTO> users;
    public List<Distributions> distributions;
}

