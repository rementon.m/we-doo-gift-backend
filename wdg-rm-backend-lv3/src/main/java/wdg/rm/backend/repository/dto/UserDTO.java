package wdg.rm.backend.repository.dto;

import javax.persistence.*;
import java.util.List;

@Entity
public class UserDTO {
    public @Id
    int id;

    @Column
    @ElementCollection(targetClass=Amounts.class)
    @OneToMany(cascade = {CascadeType.ALL})
    public List<Amounts> balance;

    public UserDTO(int id, List<Amounts> balance) {
        this.id = id;
        this.balance = balance;
    }

    public UserDTO() {}
}

