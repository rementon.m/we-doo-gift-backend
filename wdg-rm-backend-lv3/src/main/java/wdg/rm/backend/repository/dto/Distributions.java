package wdg.rm.backend.repository.dto;

public class Distributions {
    int id;
    int amount;
    String start_date;
    String end_date;
    int company_id;
    int user_id;

    public Distributions(int id, int amount, String start_date, String end_date, int company_id, int user_id) {
        this.id = id;
        this.amount = amount;
        this.start_date = start_date;
        this.end_date = end_date;
        this.company_id = company_id;
        this.user_id = user_id;
    }
}
