package wdg.rm.backend.repository.dto;

import wdg.rm.backend.VoucherType;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class WalletDTO {
    public @Id int id;
    public String name;
    public VoucherType type;

    public WalletDTO(int id, String name, VoucherType type) {
        this.id = id;
        this.name = name;
        this.type = type;
    }

    public WalletDTO() {}
}
