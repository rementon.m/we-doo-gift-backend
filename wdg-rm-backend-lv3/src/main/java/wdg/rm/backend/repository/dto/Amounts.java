package wdg.rm.backend.repository.dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Amounts {
    @Id @GeneratedValue(strategy= GenerationType.AUTO) int id;
    public int wallet_id;
    public int amount;

    public Amounts(){}

    public Amounts(int wallet_id, int amount) {
        this.wallet_id = wallet_id;
        this.amount = amount;
    }
}
