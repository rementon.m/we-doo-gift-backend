package wdg.rm.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import wdg.rm.backend.repository.dto.WalletDTO;

public interface WalletRepository extends JpaRepository<WalletDTO, Long> { }