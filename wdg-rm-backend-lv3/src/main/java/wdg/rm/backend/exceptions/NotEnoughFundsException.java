package wdg.rm.backend.exceptions;

public class NotEnoughFundsException extends RuntimeException {
    public NotEnoughFundsException() {
        super("Not enough funds!");
    }
}

