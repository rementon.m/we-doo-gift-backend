package wdg.rm.backend.exceptions;

public class UserNotFoundException extends RuntimeException {
    public UserNotFoundException(int id) {
        super("The user " + id + " could not be find!");
    }
}