package wdg.rm.backend;

import wdg.rm.backend.elements.Company;
import wdg.rm.backend.elements.User;
import wdg.rm.backend.elements.Vouchers;
import org.junit.jupiter.api.Test;
import wdg.rm.backend.exceptions.NotEnoughFundsException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static wdg.rm.backend.VoucherType.GIFT;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CompanyTest {

    private Date today = Calendar.getInstance().getTime();

    @Test
    void companyDistribute100eGiftCardToUser() {
        Company company = new Company("company",200);

        List<Vouchers> vouchersList = new ArrayList<>();
        vouchersList.add(new Vouchers(345,today,GIFT));

        User user = new User(vouchersList);

        company.distributeVouchers(user,100,GIFT);

        assertEquals(445,user.getBalanceFor(GIFT));
        assertEquals(100,company.getBalance());
    }

    @Test
    void companyDistribute100eGiftCardToUserWithSameAmountBalance() {
        Company company = new Company("company",100);

        List<Vouchers> vouchersList = new ArrayList<>();
        vouchersList.add(new Vouchers(345,today,GIFT));

        User user = new User(vouchersList);

        company.distributeVouchers(user,100,GIFT);

        assertEquals(445,user.getBalanceFor(GIFT));
        assertEquals(0,company.getBalance());
    }

    @Test
    void companyCanNotDistribute100eGiftCardToUser()  {
        Company company = new Company("company",90);

        List<Vouchers> vouchersList = new ArrayList<>();
        vouchersList.add(new Vouchers(345,today,GIFT));

        User user = new User(vouchersList);

        assertThrows(NotEnoughFundsException.class, () -> {
            company.distributeVouchers(user,100,GIFT);
        });

        assertEquals(345,user.getBalanceFor(GIFT));
        assertEquals(90,company.getBalance());
    }


    @Test
    void companyDistributeMultipleAmountGiftCardToUser() {
        Company company = new Company("company",340);

        List<Vouchers> vouchersList = new ArrayList<>();
        vouchersList.add(new Vouchers(5,today,GIFT));

        User user = new User(vouchersList);

        company.distributeVouchers(user,100,GIFT);
        assertEquals(105,user.getBalanceFor(GIFT));
        assertEquals(240,company.getBalance());

        company.distributeVouchers(user,50,GIFT);
        assertEquals(155, user.getBalanceFor(GIFT));
        assertEquals(190, company.getBalance());

        assertThrows(NotEnoughFundsException.class, () -> {
            company.distributeVouchers(user,300,GIFT);
        });
        assertEquals(155,user.getBalanceFor(GIFT));
        assertEquals(190,company.getBalance());

        company.distributeVouchers(user,150,GIFT);
        assertEquals(305,user.getBalanceFor(GIFT));
        assertEquals(40,company.getBalance());
    }

}