package main.java.elements;

import main.java.helpers.DateHelper;
import main.java.helpers.DateHelperImpl;

import java.util.Calendar;
import java.util.Date;

public class Company {
    private String name;
    private int balance;
    private DateHelper dateHelper;

    public Company(String name, int balance) {
        this.name = name;
        this.balance = balance;
        this.dateHelper = new DateHelperImpl();
    }

    public int getBalance() {
        return balance;
    }

    public void distributeGiftCard(User user, int amount) {
        if(this.balance - amount >= 0) {
            user.getGiftCard(createGiftCard(amount,dateHelper.getToday()));
            this.balance = this.balance - amount;
        }
    }

    public GiftCard createGiftCard(int amount, Date startDate){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);
        calendar.add(Calendar.YEAR, 1);
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        Date endDate = calendar.getTime();
        return new GiftCard(amount,startDate,endDate);
    }
}
