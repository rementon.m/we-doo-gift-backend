package main.java.elements;

import java.util.ArrayList;

public class User {
    private final int balance;
    private final ArrayList<GiftCard> giftCards = new ArrayList<>();

    public User(int initialBalance) {
        this.balance = initialBalance;
    }

    public int getBalance() {
        int actualAmount = 0;
        for(GiftCard card : giftCards){
           actualAmount += card.getAmount();
        }
        return balance + actualAmount;
    }

    public void getGiftCard(GiftCard card) {
        giftCards.add(card);
    }


}