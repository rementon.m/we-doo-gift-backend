package main.java.elements;

import main.java.helpers.DateHelper;
import main.java.helpers.DateHelperImpl;

import java.util.Date;

public class GiftCard {
    private int amount;
    private DateHelper dateHelper = new DateHelperImpl();
    private Date startDate;
    private Date endDate;

    public GiftCard(int amount, Date startDate, Date endDate, DateHelper dateHelper) {
        this.amount = amount;
        this.dateHelper = dateHelper;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public GiftCard(int amount, Date startDate, Date endDate) {
        this.amount = amount;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public int getAmount(){
        if(!startDate.after(dateHelper.getToday()) && !endDate.before(dateHelper.getToday())) return amount;
        else return 0;
    }

    public Date getEndDate() {
        return endDate;
    }
}
