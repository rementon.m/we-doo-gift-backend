package main.java.helpers;

import main.java.elements.Company;
import main.java.elements.User;
import main.java.helpers.dto.CompanyDTO;
import main.java.helpers.dto.Elements;
import main.java.helpers.dto.UserDTO;

import java.util.ArrayList;
import java.util.List;

public class JsonHelper {

    public List<Company> getCompanies(Elements elements){
        List<Company> companies = new ArrayList<>();
        for(CompanyDTO company: elements.companies){
            Company newCompany = new Company(company.name, company.balance);
            companies.add(newCompany);
        }
        return companies;
    }

    public List<User> getUsers(Elements elements){
        List<User> users = new ArrayList<>();
        for(UserDTO user: elements.users){
            User newUser = new User(user.balance);
            users.add(newUser);
        }
        return users;
    }

}