package main.java.helpers.dto;

import java.util.List;

public class Elements {
    public List<CompanyDTO> companies;
    public List<UserDTO> users;
    public List<Distributions> distributions;
}

