package main.java.actions;

import main.java.elements.Company;
import main.java.elements.User;

public class CardDistributionService {

    public void distributeGiftCard(Company company, User user, int amount){
        company.distributeGiftCard(user, amount);
    }
}
