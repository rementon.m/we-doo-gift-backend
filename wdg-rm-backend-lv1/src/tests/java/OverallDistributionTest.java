package tests.java;

import main.java.actions.CardDistributionService;
import main.java.elements.Company;
import main.java.elements.User;
import main.java.helpers.FileService;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class OverallDistributionTest {

    CardDistributionService cardDistributionService = new CardDistributionService();

    FileService fileService = new FileService("input.json");

    @Test
    void firstCompanyGives100eGiftCardTOFirstUser() {
        Company company = fileService.getCompanies().get(0);
        User user = fileService.getUsers().get(0);

        cardDistributionService.distributeGiftCard(company,user,100);

        assertEquals(200,user.getBalance());
        assertEquals(900,company.getBalance());

    }
}
