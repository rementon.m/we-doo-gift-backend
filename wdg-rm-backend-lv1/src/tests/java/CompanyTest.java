package tests.java;

import main.java.elements.Company;
import main.java.elements.User;
import main.java.helpers.DateHelper;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Calendar;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CompanyTest {

    @Test
    void companyDistribute100eGiftCardToUser() {
        Company company = new Company("company",200);
        User user = new User(345);

        company.distributeGiftCard(user,100);

        assertEquals(445,user.getBalance());
        assertEquals(100,company.getBalance());
    }

    @Test
    void companyDistribute100eGiftCardToUserWithSameAmountBalance() {
        Company company = new Company("company",100);
        User user = new User(345);

        company.distributeGiftCard(user,100);

        assertEquals(445,user.getBalance());
        assertEquals(0,company.getBalance());
    }

    @Test
    void companyCanNotDistribute100eGiftCardToUser() {
        Company company = new Company("company",90);
        User user = new User(345);

        company.distributeGiftCard(user,100);

        assertEquals(345,user.getBalance());
        assertEquals(90,company.getBalance());
    }

    @Test
    void companyDistributeMultipleAmountGiftCardToUser() {
        Company company = new Company("company",340);
        User user = new User(5);

        company.distributeGiftCard(user,100);
        assertEquals(105,user.getBalance());
        assertEquals(240,company.getBalance());

        company.distributeGiftCard(user,50);
        assertEquals(155, user.getBalance());
        assertEquals(190, company.getBalance());

        company.distributeGiftCard(user,200);
        assertEquals(155,user.getBalance());
        assertEquals(190,company.getBalance());

        company.distributeGiftCard(user,150);
        assertEquals(305,user.getBalance());
        assertEquals(40,company.getBalance());
    }



    /*** GIFT CARD CREATION TESTS ****/

    @Test
    void createdGiftCardHas365DaysValidity() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2020);
        cal.set(Calendar.MONTH, Calendar.DECEMBER);
        cal.set(Calendar.DAY_OF_MONTH, 4);
        Date initial = cal.getTime();

        cal.set(Calendar.YEAR, 2021);
        cal.set(Calendar.MONTH, Calendar.DECEMBER);
        cal.set(Calendar.DAY_OF_MONTH, 3);
        Date expected = cal.getTime();

        final DateHelper dateHelper = Mockito.mock(DateHelper.class);
        Mockito.when(dateHelper.getToday()).thenReturn(initial);

        Company company = new Company("",400);

        assertEquals(expected,company.createGiftCard(400,initial).getEndDate());
    }

    @Test
    void createdGiftCardHas365DaysValidity31thCase() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2020);
        cal.set(Calendar.MONTH, Calendar.JANUARY);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        Date initial = cal.getTime();

        cal.set(Calendar.YEAR, 2020);
        cal.set(Calendar.MONTH, Calendar.DECEMBER);
        cal.set(Calendar.DAY_OF_MONTH, 31);
        Date expected = cal.getTime();

        final DateHelper dateHelper = Mockito.mock(DateHelper.class);
        Mockito.when(dateHelper.getToday()).thenReturn(initial);

        Company company = new Company("",400);

        assertEquals(expected,company.createGiftCard(400,initial).getEndDate());
    }
}
