package tests.java;

import main.java.elements.GiftCard;
import main.java.elements.User;
import main.java.helpers.DateHelper;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Calendar;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UserTest {

    @Test
    void initializedUserWithEmptyGiftCardsHasCorrectAmount() {
        User user = new User(0);
        assertEquals(0,user.getBalance());
    }

    @Test
    void initializedUserWithInitialAmountHasCorrectAmount() {
        int initialAmount = 5697593;
        User user = new User(initialAmount);

        assertEquals(initialAmount,user.getBalance());
    }

    @Test
    void userAddValidGiftCardReturnCorrectAmount() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2020);
        cal.set(Calendar.MONTH, Calendar.DECEMBER);
        cal.set(Calendar.DAY_OF_MONTH, 4);
        Date start = cal.getTime();

        cal.set(Calendar.YEAR, 2021);
        cal.set(Calendar.MONTH, Calendar.MARCH);
        cal.set(Calendar.DAY_OF_MONTH, 15);
        Date end = cal.getTime();

        cal.set(Calendar.YEAR, 2021);
        cal.set(Calendar.MONTH, Calendar.MARCH);
        cal.set(Calendar.DAY_OF_MONTH, 2);
        Date today = cal.getTime();

        final DateHelper dateHelper = Mockito.mock(DateHelper.class);
        Mockito.when(dateHelper.getToday()).thenReturn(today);

        User user = new User(5000);
        user.getGiftCard(new GiftCard(100,start,end,dateHelper));

        assertEquals(5100,user.getBalance());
    }

}